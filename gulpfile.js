var browserify = require('browserify')
var gulp = require('gulp')
var source = require('vinyl-source-stream')
const babelify = require('babelify')

// task para executar o browserify
gulp.task('browserify', () => {
  return browserify('./src/app')
    .transform(
      // compilar codigo
      babelify.configure()
    )
    .bundle()
    .pipe(source('app.js')) // cria o novo arquivo app.js
    .pipe(gulp.dest('./public')) // caminho da pasta
})

// task para automatizar a geração/atualização de um novo arquivo app.js

gulp.task('watch', () => {
  gulp.watch('./src/*', gulp.series('browserify'))
})
