import Vue from 'vue/dist/vue.common'
import sayHello from './say_hello'

var app = new Vue({
  el: '#app',
  data: {
    message: sayHello('IDW')
  }
})
